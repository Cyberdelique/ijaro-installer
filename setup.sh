#!/bin/bash
#########################################################################################
##                                    update                                           ##
#########################################################################################
yes | sudo pacman -Syyu

#########################################################################################
##                               virtualisation                                        ##
#########################################################################################

# download virtual stuffs
yes | sudo pacman -S virt-manager virt-viewer qemu vde2 ebtables dnsmasq bridge-utils openbsd-netcat cool-retro-term pulseaudio

# add current user to libvirt group
sudo usermod -aG libvirt $USER

# start libvirt
sudo systemctl enable libvirtd.service
sudo systemctl start libvirtd.service

# enable network
sudo cp -R ./etc/* /etc/
yes | cp -f ./.config/nitrogen/bg-saved.cfg ~/.config/nitrogen/bg-saved.cfg
#########################################################################################
##                                 GUI setup                                           ##
#########################################################################################

git clone https://gitlab.com/cyberdelique/i3-cyber-settings.git
cd i3-cyber-settings
yes | cp -f ./config ~/.i3/config

#########################################################################################
##                           creating VMs mountpoint                                   ##
#########################################################################################

sudo mkdir /VMs && sudo mkdir /VMs/HDs && sudo mkdir /VMs/ISOs
sudo chown $USER /VMs/ISOs/

#########################################################################################
##                          creating NFSs mountpoint                                   ##
#########################################################################################

sudo mkdir /NFSshare
sudo chown $USER /NFSshare


